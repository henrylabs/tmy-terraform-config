terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
    region = "eu-central-1"
}




resource "aws_vpc" "my_vpc" {
    cidr_block = var.vpc_variables
    tags = {
      Name = "${var.name-tag}-vpc"
      }
}

resource "aws_subnet" "my_subnet" {
    vpc_id = aws_vpc.my_vpc.id
    cidr_block = var.subnet_info
    availability_zone = var.availability_zone
    tags = {
      Name = "${var.name-tag}-subnet"
      
    }
}

data "aws_vpc" "existing_vpc" {
    default = true
}

resource "aws_subnet" "my_subnet2" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = var.subnet_info-2
    availability_zone = var.availability_zone
   
}

resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.my_vpc.id
    
}

resource "aws_default_route_table" "myapp-rt" {
    default_route_table_id = aws_vpc.my_vpc.default_route_table_id
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
  
}

resource "aws_route_table_association" "myapp-rt-assc" {
    subnet_id = aws_subnet.my_subnet.id
    route_table_id = aws_default_route_table.myapp-rt.id
}

resource "aws_default_security_group" "myapp-sg" {
    vpc_id = aws_vpc.my_vpc.id
    

    ingress  {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.ingress-ip]
        description = "myapp inbound security group created using terraform"
        
    }
    
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        description = "myapp outbound security group created using terraform"
    }
}
   
data "aws_ami" "server-image" {
    most_recent = true
    owners = ["amazon"]
    
    filter {
        name = "name"
        values = [ "amzn2-ami-hvm-*-x86_64-gp2" ]
        }
    
    filter {
         name = "virtualization-type"
         values = [ "hvm" ]
    }
}

resource "aws_key_pair" "server-key-pair" {
    key_name = "my-server-key"
    public_key = file(var.key-location)
}

resource "aws_instance" "my-server" {
    ami = data.aws_ami.server-image.id
    instance_type = var.instance-type
    subnet_id = aws_subnet.my_subnet.id
    associate_public_ip_address = true 
    vpc_security_group_ids = [ aws_default_security_group.myapp-sg.id ]
    availability_zone = var.availability_zone
    key_name = aws_key_pair.server-key-pair.key_name
    user_data = file("config.sh")

    tags = {
       Name = "${var.name-tag}-server"
    }

}
output "instance-ip-address" {
    value = aws_instance.my-server.public_ip
}

 

    
